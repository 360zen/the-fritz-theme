<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package The_Fritz_Theme
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<a href="/music" title="Jonny Fritz - Music">
				<div id="home-left">
						<div class="textwrap">
							<h2>Jonny Fritz</h2>
							<h1>Music</h1>
						</div>
				</div>
			</a>
			<a href="/leather-work/order" title="Jonny Fritz - Music">
				<div id="home-right">
						<div class="textwrap">
							<h2>Dad Country</h2>
							<h1>Leather</h1>
						</div>
				</div>
			</a>

			<?php
//			while ( have_posts() ) : the_post();
//
//				get_template_part( 'template-parts/content', 'page' );
//
//				// If comments are open or we have at least one comment, load up the comment template.
//				if ( comments_open() || get_comments_number() ) :
//					comments_template();
//				endif;
//
//			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
//get_footer();
	wp_footer();
