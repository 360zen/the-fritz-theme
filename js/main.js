/**
 * Created by justin on 3/8/16.
 */
( function() {

    window.onload = function(){




    };

    function setBackgroundAndHeight(){
        var body = document.body;
        var page = document.getElementById('page');
        var mobileBgDiv = document.getElementById('mobile-bg');

        var height = window.innerHeight;
        //if it's not the home page, set the top margin
        if ( -1 === body.className.indexOf( 'home' )) {
            var windowSize = jQuery(window).width();
            var topMargin;
            if (windowSize <= 479) {
                topMargin = ((height/4) * 3) + 'px';
                page.style.marginTop = topMargin;
                mobileBgDiv.style.marginBottom = (height/4);
            } else {
                topMargin = (height - 200) + 'px';
                page.style.marginTop = topMargin;
                mobileBgDiv.style.marginBottom = 200 + 'px';
                console.log(mobileBgDiv);
            }

        }
        //fix for fullscreen background on iOS
        var bgImage = jQuery('body').css('background-image');
        mobileBgDiv.style.backgroundImage = bgImage;

    }
    jQuery(document).ready(function(){
        setBackgroundAndHeight();

        jQuery('.gfield_description').each(function(){
            //jQuery(this).closest('.gfield').prepend('<a href="#" class="tooltip-trigger"><i class="fa fa-info-circle"></i></a>');
            jQuery('<a href="#" class="tooltip-trigger"><i class="fa fa-info-circle"></i></a>').insertBefore(this);
        });
        jQuery(document).on('click','.tooltip-trigger', function(e){
            e.preventDefault();

            if (jQuery(this).closest('.gfield').hasClass('tooltipped')) {
                jQuery(this).closest('.gfield').toggleClass('tooltipped');
            } else {
                jQuery('.tooltipped').each(function(){
                    jQuery(this).removeClass('tooltipped');
                });
                jQuery(this).closest('.gfield').toggleClass('tooltipped');
            }

        });

    });

    jQuery(window).resize(function(){
        setBackgroundAndHeight();
    });

} )();