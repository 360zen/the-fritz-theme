<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package The_Fritz_Theme
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<!--TYPEKIT FONTS-->
<script src="https://use.typekit.net/ywm3wsy.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
<!--END TYPEKIT FONTS-->

<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="breakpoint" content="small" media="(max-width: 768px)">
<meta name="breakpoint" content="medium" media="(min-width: 769px) and (max-width: 1024px)">
<meta name="breakpoint" content="medium-up" media="(min-width: 769px)">
<meta name="breakpoint" content="large" media="(min-width: 1025px) and (max-width: 1399px)">
<meta name="breakpoint" content="wide" media="(min-width: 1400px)">
<meta name="breakpoint" content="retina" media="(-webkit-min-device-pixel-ratio: 2)">

<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="mobile-bg"></div>
<a href="javascript:void(0)" id="hamburger" aria-controls="primary-menu" aria-expanded="false"><i class="fa fa-bars"></i></a>
<div id="page" class="site">
	<?php if (!is_front_page()): ?>
<!--	<a href="javascript:void(0)" id="hamburger" aria-controls="primary-menu" aria-expanded="false"><i class="fa fa-bars"></i></a>-->
	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<div id="header-social">
				<a href="http://dadcountryleather.tumblr.com/"><i class="fa fa-tumblr"></i></a>
				<a href="https://www.facebook.com/dadcountry/"><i class="fa fa-facebook"></i></a>
				<a href="https://twitter.com/dadcountry"><i class="fa fa-twitter"></i></a>
				<a href="http://instagram.com/_u/dadcountry/"><i class="fa fa-instagram"></i></a>
				<a href="https://www.youtube.com/channel/UCzuOpLU_MRxOfQoIScz7MlA/featured"><i class="fa fa-youtube"></i></a>
			</div>
			<?php include_once "mailchimp-form.php"; ?>

		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->
	<?php endif; ?>

	<div id="content" class="site-content">
