<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package The_Fritz_Theme
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<span>Copyright &copy; 2016 &middot; All Rights Reserved &middot; <a href="http://jonnyfritz.com/" >Jonny Fritz</a></span>
			<span class="sep"> | </span>
			<a href="/contact">Contact</a>
			<span class="sep"> | </span>
			<a href="/press">Press</a>
			<a href="http://360zen.com" class="zenlove">Website by 360 Zen</a>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
